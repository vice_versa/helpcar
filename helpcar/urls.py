# -*- coding: utf-8 -*-

from django.conf.urls import include, url

from django.conf import settings
from django.contrib import admin

from helpcar.apps.main import urls as main_urls


urlpatterns = [
    #url(r'^grappelli/', include('grappelli.urls')),
    url(r'^admin/', include(admin.site.urls)),
]

urlpatterns += [
    url(r'^', include(main_urls)),
]


if settings.DEBUG:
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns
    from django.conf.urls.static import static

    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
