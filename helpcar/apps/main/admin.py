# -*- coding: utf-8 -*-

from django.contrib import admin

from .models import Post, PostComment, PostImage


class PostCommentTabularInline(admin.TabularInline):
    model = PostComment


class PostImageTabularInline(admin.TabularInline):
    model = PostImage


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    inlines = (PostCommentTabularInline, PostImageTabularInline)
