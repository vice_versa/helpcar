# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models


class Post(models.Model):

    title = models.CharField(
        verbose_name=u'Заголовок',
        max_length=255)

    content = models.TextField(
        verbose_name=u'Содержание'
    )

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    client = models.ForeignKey(
        'client.Client',
        verbose_name=u'Клиент',
        blank=True,
        null=True)

    class Meta:
        verbose_name = u'Пост'
        verbose_name_plural = u'Посты'
        ordering = ('-created_at',)

    def __unicode__(self):
        return "%s %s" % (self.title, unicode(self.client))


class PostImage(models.Model):

    image = models.ImageField(
        verbose_name=u'Картинка',
        upload_to='images/cars')

    order = models.PositiveIntegerField(
        verbose_name=u'Порядок',
        default=0)

    post = models.ForeignKey(
        'Post',
        verbose_name=U'Пост')

    class Meta:
        verbose_name = u'Картинка к Посту'
        verbose_name_plural = u'Картинки к посту'
        ordering = ('order',)

    def __unicode__(self):
        return "%s" % (self.post)


class PostComment(models.Model):

    post = models.ForeignKey(
        'Post',
        verbose_name=U'Пост',
        blank=True,
        null=True)

    content = models.TextField(
        verbose_name=u'Содержание'
    )

    owner = models.ForeignKey(
        User,
        verbose_name=u'Пользователь',
        blank=True,
        null=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = u'Коментарий к посту'
        verbose_name_plural = u'Комментарии к посту'
        ordering = ('-created_at',)

    def __unicode__(self):
        return "%s" % (self.post)
