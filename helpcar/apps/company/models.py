# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models


class Company(models.Model):

    is_active = models.BooleanField(
        verbose_name=u'Активный?',
        default=False
    )

    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE
    )

    class Meta:
        verbose_name = u'Компания'
        verbose_name_plural = u'Компании'

    def __unicode__(self):

        return "%s" % (unicode(self.user))
