# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models


class Client(models.Model):
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE
    )

    class Meta:
        verbose_name = u'Клиент'
        verbose_name_plural = u'Клиенты'

    def __unicode__(self):

        return "%s" % (unicode(self.user))
